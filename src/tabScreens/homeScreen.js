import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, Modal } from 'react-native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { useFonts } from 'expo-font'; 
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome6 } from '@expo/vector-icons';

const SimplePage = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const openModal = () => {
    setModalVisible(true);
  };

  // Função para fechar o modal
  const closeModal = () => {
    setModalVisible(false);
  };

  const perfil = () => {
    navigation.navigate('Perfil');
  }


  const Paris = require('../../assets/suite8.jpg')
  const Atenas = require('../../assets/suite7.jpeg')
  const Dublin = require('../../assets/suite6.jpg')
  const Londres = require('../../assets/suite5.jpg')
  const Toronto = require('../../assets/suite4.jpg')
  const Sidney = require('../../assets/suite3.jpg')
  const Chicago = require('../../assets/suite2.jpg')
  const Barcelona = require('../../assets/suite.jpg')

  const [loaded] = useFonts({
    poppinsNormal: require('../../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../../assets/fonts/poppins/Poppins-Medium.ttf'),
    poppinsLight: require('../../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  if (!loaded) {
    return null; 
  }

  return (
    <View style={styles.container}>
      <View style={styles.textos}>
        <Text style={styles.text}>Seja bem-vindo</Text>
        <TouchableOpacity style={styles.icone} onPress={perfil}><FontAwesome6 style={styles.icone2}  name="circle-user" size={24} color="black" /></TouchableOpacity>
        <Text style={styles.text1}>Onde você quer se hospedar hoje?</Text>
     
      </View>

      <AntDesign style={styles.iconsearch} name="search1" size={23} color="white" />
      <TextInput style={styles.search} placeholder='Procure o local ideal para você ficar.'></TextInput>
 
    <View style={styles.selecao}>

    <ScrollView style={styles.scroll} horizontal={true} showsHorizontalScrollIndicator={false}>
  
        <TouchableOpacity style={styles.button}>
        <Text style={styles.textButton}>Dubai</Text>
        </TouchableOpacity>
    
        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Atenas</Text>
        </TouchableOpacity>
     
        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Paris</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Dublin</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Chicago</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Toronto</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Londres</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
         <Text style={styles.textButton}>Barcelona</Text>
        </TouchableOpacity>

        </ScrollView>
    
      
    </View>

   <View style={styles.view}>
    <Text style={styles.suites}>Explore nossas suítes de luxo:</Text>
   </View>

   <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={closeModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>Reservar quarto</Text>
            <TextInput style={styles.modalInput} placeholder="Tipo de quarto" />
            <TextInput style={styles.modalInput} placeholder="Data de entrada" />
            <TextInput style={styles.modalInput} placeholder="Data de saída" />
            <TextInput style={styles.modalInput} placeholder="Número do quarto" />
            <TouchableOpacity style={styles.reserveButton} onPress={closeModal}>
              <Text style={styles.reserveButtonText}>Reserve</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>



    <View style={styles.selecao2}>

    <ScrollView style={styles.scroll2} horizontal={true} showsHorizontalScrollIndicator={false}>

    

       <TouchableOpacity style={styles.button2}>
         
         <Image source={Barcelona} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte Luxo</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
        
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>
    
       <TouchableOpacity style={styles.button2}>
        <Image source={Chicago} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 2</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>
     
       <TouchableOpacity style={styles.button2}>
        <Image source={Sidney} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 2</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />

          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

       <TouchableOpacity style={styles.button2}>
        <Image source={Toronto} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 3</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

       <TouchableOpacity style={styles.button2}>
        <Image source={Londres} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 4</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

       <TouchableOpacity style={styles.button2}>
        <Image source={Dublin} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 5</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

       <TouchableOpacity style={styles.button2}>
         
        <Image source={Atenas} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 6</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button2}>
        <Image source={Paris} style={styles.img}></Image>
         <Text style={styles.textButton2}>Suíte luxo 7</Text>
         <Text style={styles.textButton3}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
         <View style={styles.stars}>
          

          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star" size={24} color="#ffb300" />
          <Ionicons name="star-half" size={24} color="#ffb300" />
          <Ionicons name="star-outline" size={24} color="#ffb300" />
          
          </View>
          <TouchableOpacity style={styles.botaoReserva} onPress={openModal}>

            <Text style={styles.reserva}>Reserve aqui</Text>

          </TouchableOpacity>
        </TouchableOpacity>

        </ScrollView>
    
      
    </View>


    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171A26',
  },
  textos: {
    marginTop: 20,
    marginLeft: 20,   // Use o nome que você definiu ao importar a fonte
  },
  text: {
    fontSize: 12,
    marginTop: 0,
    fontSize: 23,
    color: '#fff',
    fontFamily: 'poppinsNormal', // Use o nome que você definiu ao importar a fonte
    letterSpacing: 1
  },  
  text1: {
    fontSize: 24,
    marginTop: 0,
    fontSize: 28,
    fontFamily:'poppinsLight',
    color: '#fff'
  },
  text2: {
    fontSize: 24,
    marginTop: 25,
    fontSize: 30,
    fontFamily:'poppinsBold',
  },
  icone: {
    position: 'absolute',
    marginTop: 0,
    color: '#fff',
    marginLeft: 420
  },
  icone2: {
    color: '#fff',

  },

  search:{
    marginTop: 10,
    borderWidth: 0.3,
    width: 400,
    marginLeft: 49,
    borderColor: 'white',
    borderRadius: 20,
    backgroundColor: 'white',
    paddingStart: 10,
    color: 'black',
    height: 40
  }, 
  iconsearch:{
    position: 'absolute',
    left: 15,
    top: 105
  },

  
  selecao: {
    marginTop: 10,
    width: 489,
    height: 70,
    paddingStart: 5,
    paddingRight: 15,
  },

  selecao2: {
    marginTop: -25,
    width: 489,
    height: 600,
    paddingStart: 0,
    paddingRight: 10,
  },

   button: {
    backgroundColor: '#5D7698',
    width: 100,
    height: 50,
    marginStart: 10,
    justifyContent: 'center',
    alignItems:'center',
    borderRadius: 10,
    marginTop: 10,
    elevation: 5
    
 
   },

   button2: {
    backgroundColor: '#1D2334',
    width: 300,
    height: 550,
    marginStart: 20,
    justifyContent: 'center',
    alignItems:'center',
    borderRadius: 10,
    marginTop: 30,
    zIndex: -10,
    elevation: 4,

 
   },
   textButton:{ 
    fontFamily: 'poppinsSemiBold',
    color: 'white'

   },

   textButton2:{ 
    fontFamily: 'poppinsLight',
    color: 'white',
    fontSize: 30,
    marginTop: 15

   },
   textButton3:{ 
    fontFamily: 'poppinsLight',
    color: 'white',
    marginStart: 10,
    marginEnd: 5

   },
   img:{
    zIndex:10,
    width: 280,
    height: 250,
    marginTop: -25,
    borderRadius: 10

   },
   stars:{
    flexDirection:'row',
    marginTop: 10
   },

   view:{
    width: 500,
    height: 70,

    marginLeft: 15,
    marginTop: 10
   },

   suites:{
    fontSize: 20,
     marginLeft: 10,
     marginTop: 15,
     fontFamily:'poppinsMedium',
     color: 'white'
   },
   modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)', // Fundo semi-transparente
  },
  modalContent: {
    backgroundColor: '#161A27',
    padding: 20,
    borderRadius: 5,
    width: 400,
    height: 400
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: 30,
    fontFamily: 'poppinsLight',
    textAlign: 'center',
    color: 'white'
  },
  modalInput: {
    borderWidth: 1,
    borderColor: 'white',
    marginBottom: 30,
    fontSize: 15,
    fontFamily: "poppinsLight",
    paddingStart: 10,
    borderRadius: 5,
    height: 33,
    backgroundColor: 'white'
    
  },
  reserveButton: {
    backgroundColor: '#1D2234',
    paddingVertical: 10,
    borderRadius: 30,
    alignItems: 'center',
  
  },
  reserveButtonText: {
    color: '#fff',
    fontSize: 16,
    fontFamily: "poppinsLight"
  },
  botaoReserva:{
    width: 200,
    backgroundColor: '#5D7698',
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 30,
    elevation: 10
  },
  reserva:{
    alignItems: 'center',
    fontFamily: "poppinsLight",
    color: 'white'

  }

 

});

export default SimplePage;
