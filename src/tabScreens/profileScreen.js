import React from 'react';
import { View, Text, StyleSheet,TouchableOpacity, Image } from 'react-native';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { useFonts } from 'expo-font';
import { Fontisto } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';



const Nilson = require('../../assets/nilson.jpg')

const SimplePage = ({ navigation }) => {

  const [loaded] = useFonts({
    poppinsNormal: require('../../assets/fonts/poppins/Poppins-Regular.ttf'),
    poppinsBold: require('../../assets/fonts/poppins/Poppins-Bold.ttf'),
    poppinsBlack: require('../../assets/fonts/poppins/Poppins-Black.ttf'),
    poppinsSemiBold: require('../../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    poppinsMedium: require('../../assets/fonts/poppins/Poppins-Medium.ttf'),
    poppinsLight: require('../../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  if (!loaded) {
    return null; 
  }


  const logout = () => {
    navigation.navigate('Home');
  }
  return (
    <View style={styles.container}>

        <TouchableOpacity style={styles.logoutContainer} onPress={logout}>
            <MaterialIcons name="logout" size={24} color="white" />
            </TouchableOpacity>

            <View style={styles.texto}>
            <Text style={styles.texto2}>Perfil</Text>
            </View>

          <View style={styles.card}>
            
      <Text style={styles.pessoa}>Nilson Izaías Papinho</Text>
      <Text style={styles.pessoa1}><Fontisto name="map-marker-alt" size={19} color="white" />  Franca - São Paulo</Text>
      <TouchableOpacity style={styles.button}>

      <Text style={styles.pessoa}>Nilson Izaías Papinho</Text>

        <Image source={Nilson} style={styles.img}></Image>

        </TouchableOpacity>

        <Text style={styles.pessoa2}>Informações</Text>

        <TouchableOpacity style={styles.botao2}><FontAwesome5 name="phone-alt" size={15} color="white" /></TouchableOpacity>
        <TouchableOpacity style={styles.botao3}><AntDesign name="idcard" size={15} color="white" /></TouchableOpacity>

        <Text style={styles.telefone}> 55+  16 99394-1612</Text>
        <Text style={styles.idUser}> Id de usuário:  0000-01</Text>

        <Text style={styles.sobre}>Sobre mim:</Text>

        <Text style={styles.sobre2}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>

        <Text style={styles.reservas}>Reservas</Text>

        </View>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171A26',
  },

  texto:{
    width: 400,
    marginLeft: 30,
    marginTop: -40
  },

  texto2:{

    color:'white',
    fontFamily: 'poppinsLight',
    fontSize: 30,
  

  },
  card:{
    marginTop: 20,
    backgroundColor: '#1F2333',
    width: 450,
    height: 690,
    marginLeft: 20,
    marginEnd: 20,
    elevation: 10,
    borderRadius: 10,


  },

  pessoa:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 24,
    marginLeft: 170,
    marginTop: 50,
    letterSpacing: 0.3

  },

  pessoa1:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 15,
    marginLeft: 210,
    marginTop: 85,
    letterSpacing: 0.3,
    opacity: 0.3

  },

  telefone:{
    position: 'absolute',
    color: 'white',
    top: 205,
    left: 65,
    fontFamily: 'poppinsLight'
  },

  idUser:{
    position: 'absolute',
    color: 'white',
    top: 255,
    left: 65,
    fontFamily: 'poppinsLight'
  },

  pessoa2:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 18,
    marginLeft: 27,
    marginTop: 150,
    letterSpacing: 0.3

  },

  sobre:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 18,
    marginLeft: 27,
    marginTop: 320,
    letterSpacing: 0.3

  },

  sobre2:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 13,
    marginLeft: 27,
    marginTop: 350,
    letterSpacing: 0.3, 
    marginEnd: 16,
    textAlign: 'justify'

  },

  reservas:{
    position: 'absolute',
    fontFamily: 'poppinsLight',
    color:'white',
    fontSize: 22,
    marginLeft: 179,
    marginTop: 490,
    letterSpacing: 0.3

  },

  botao2:{

    width: 27,
    height: 27,
    backgroundColor: '#2F344C',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 200,
    left: 27,
    borderRadius: 10,
    elevation: 5

  },

  botao3:{

    width: 27,
    height: 27,
    backgroundColor: '#2F344C',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 250,
    left: 27,
    borderRadius: 10,
    elevation: 5

  },

  


button:{
  margin: 23,
  elevation: 10,
  borderWidth: 3,
  borderColor: '#6773A2',
  width:104,
  justifyContent:'center',
  alignItems: 'center',
  height: 104,
  borderRadius: 100
},

img:{

  width: 100,
  height: 100,
  borderRadius: 50,
  borderWidth: 0.1,
  borderColor: 'white',

},

  logoutContainer:{
    marginLeft: 430,
    marginTop: 30,
    height:40,
    width:60,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:15,

  },
 
});

export default SimplePage;
